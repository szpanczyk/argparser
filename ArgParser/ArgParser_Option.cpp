#include "ArgParser.h"


ArgParser::Option::Option(char identifier)
	: type(SHORT), identifier(std::string(1, identifier))
{
}

ArgParser::Option::Option(std::string identifier)
	: type(LONG), identifier(identifier)
{
}

bool ArgParser::Option::operator< (Option const &  rhs) const
{
	if (this->type == rhs.type) return this->identifier < rhs.identifier;

	return this->type < rhs.type;
}
