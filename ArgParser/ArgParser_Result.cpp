#include "ArgParser.h"

ArgParser::Result::Result()
	: times(0), option_argument("")
{
}

int ArgParser::Result::getTimes()
{
	return this->times;
}

void ArgParser::Result::increaseTimes()
{
	++this->times;
}

void ArgParser::Result::setArgument(std::string argument)
{
	this->option_argument = argument;
}

std::string ArgParser::Result::getArgument()
{
	return this->option_argument;
}
