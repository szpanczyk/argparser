#include "ArgParser.h"

ArgParser::ArgParser()
{
}

ArgParser::~ArgParser()
{
}

bool ArgParser::isHandleRegistered(std::string handle)
{
	return this->records.count(handle) == 1;
}

bool ArgParser::isOptionRegistered(Option option)
{
	return this->handles.count(option) == 1;
}

std::string ArgParser::getHandle(Option option)
{
	if (!isOptionRegistered(option)) throw std::runtime_error("Unregistered option.");
	return (*this->handles.find(option)).second;
}

ArgParser::Record & ArgParser::accessRecord(std::string handle)
{
	return this->records.find(handle)->second;
}

ArgParser::Record & ArgParser::accessRecordByOption(Option option)
{
	return accessRecord(getHandle(option));
}

void ArgParser::RegisterClause(Clause clause)
{
	if (isHandleRegistered(clause.getHandle()))
		throw std::runtime_error("Unregistered handle.");

	for (Option option : clause.getOptions())
	{
		if (isOptionRegistered(option))
			throw std::runtime_error("Options already registered.");
	}

	// at this point we're sure everything is fine

	for (Option option : clause.getOptions())
	{
		this->handles.insert(std::make_pair(option, clause.getHandle()));
	}
	this->records.insert(std::make_pair(clause.getHandle(), std::make_pair(clause, Result())));
}

//TODO deal with cases like command -abc "-- i can use whitespaces in long option"
void ArgParser::Parse(int argc, char * argv[])
{
	std::queue<std::string> args;

	for (int i = 1; i < argc; ++i)
	{
		args.push(argv[i]);
	}

	while (!args.empty())
	{
		std::string arg(args.front());

		if (arg == "--")
		{
			args.pop();
			break;
		}
		else  if (arg.length() > 2 && arg.substr(0, 2) == "--")
		{
			std::string option = arg.substr(2, arg.length() - 2);
			args.pop();
			ParseOptionWithArgument(option, args);
		}
		else if (arg.length() > 1 && arg.substr(0, 1) == "-")
		{
			for (char option : arg.substr(1, arg.length() - 2))
			{
				ParseOptionNoArgument(option);
			}

			char option = arg.at(arg.length() - 1);
			args.pop();
			ParseOptionWithArgument(option, args);
		}
		else
		{
			break;
		}
	}
	while (!args.empty())
	{
		addOperand(args.front()); // not sure how this will work
		args.pop();
	}
}

void ArgParser::ParseOptionNoArgument(Option option)
{
	if (!isOptionRegistered(option)) throw std::runtime_error("Unregistered option.");
	Clause::Traits traits = getClauseTraitsByOption(option);
	if (traits.argument_mandatory) throw std::runtime_error("Argument required and absent for option.");
	if (!traits.multiple_occurences && getOptionTimesByOption(option) > 0) throw std::runtime_error("Second use of option.");
	incOptionTimes(option);
}

void ArgParser::ParseOptionWithArgument(Option option, std::queue<std::string>& queue )
{
	if (!isOptionRegistered(option)) throw std::runtime_error("Unregistered option.");
	Clause::Traits traits = getClauseTraitsByOption(option);
	if (!traits.multiple_occurences && getOptionTimesByOption(option) > 0) throw std::runtime_error("Second use of option.");
	if (traits.argument_mandatory && queue.empty()) throw std::runtime_error("Argument required and absent for option.");

	if (traits.argument_mandatory)
	{
		std::string argument(queue.front());
		queue.pop();
		setOptionArgument(option, argument);
	}
	incOptionTimes(option);
}

ArgParser::Clause::Traits ArgParser::getClauseTraits(std::string handle)
{
	return accessRecord(handle).first.getTraits();
}

ArgParser::Clause::Traits ArgParser::getClauseTraitsByOption(Option option)
{
	return accessRecordByOption(option).first.getTraits();
}

int ArgParser::getOptionTimes(std::string handle)
{
	return accessRecord(handle).second.getTimes();
}

int ArgParser::getOptionTimesByOption(Option option)
{
	return accessRecordByOption(option).second.getTimes();
}

std::string ArgParser::getOptionArgument(std::string handle)
{
	return accessRecord(handle).second.getArgument();
}

std::vector<std::string> ArgParser::getOperands()
{
	return this->operands;
}

void ArgParser::addOperand(std::string operand)
{
	this->operands.push_back(operand);
}

void ArgParser::incOptionTimes(Option option)
{
	accessRecordByOption(option).second.increaseTimes();
}

void ArgParser::setOptionArgument(Option option, std::string argument)
{
	accessRecordByOption(option).second.setArgument(argument);
}
