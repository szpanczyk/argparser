#include "ArgParser.h"

ArgParser::Clause::Clause(std::string handle, std::vector<char> short_options, std::vector<std::string> long_options, Traits traits)
	: handle(handle), traits(traits)
{
	this->options.insert(options.end(), short_options.begin(), short_options.end());
	this->options.insert(options.end(), long_options.begin(), long_options.end());
}

std::string ArgParser::Clause::getHandle()
{
	return this->handle;
}

std::vector<ArgParser::Option> ArgParser::Clause::getOptions()
{
	return options;
}

ArgParser::Clause::Traits ArgParser::Clause::getTraits()
{
	return this->traits;
}
