#pragma once

#include <map>
#include <vector>
#include <queue>
#include <string>

class ArgParser
{
public:
	class Option
	{
		enum OptionType { SHORT, LONG } type;
		std::string identifier;
	public:
		Option(char identifier);
		Option(std::string identifier);

		bool operator <  (Option const &  rhs) const;
	};

	class Clause
	{
	public:
		struct Traits
		{
			bool argument_mandatory;
			bool multiple_occurences;
			//TODO construction and validation of such bool class with multiple constructors?
			//Traits();
			//Traits(bool argument_mandatory, bool multiple_occurences);
		};
	private:
		// TODO handle by dedicated Handle type?
		std::string handle;
		std::vector<Option> options;
		Traits traits;
	public:
		Clause(std::string handle, std::vector<char> short_options, std::vector<std::string> long_options, Traits traits);
		std::string getHandle();
		std::vector<Option> getOptions();
		Traits getTraits();
	};

	class Result
	{
		int times;
		std::string option_argument;
	public:
		Result();
		int getTimes();
		void increaseTimes();
		void setArgument(std::string argument);
		std::string getArgument();
	};

	//TODO poor nam

	typedef std::pair<Clause, Result> Record;
private:
	// perhaps I should create a class Handle for outside use?
	std::map<Option, std::string> handles;
	std::map<std::string, Record> records;
	std::vector<std::string> operands;

	bool isHandleRegistered(std::string handle);
	bool isOptionRegistered(Option option);
	std::string getHandle(Option option);

	void addOperand(std::string operand);

	void incOptionTimes(Option option);
	void setOptionArgument(Option option, std::string argument);
	Record& accessRecord(std::string handle);
	Record& accessRecordByOption(Option option);

public:
	ArgParser();
	~ArgParser();

	void RegisterClause(Clause clause);
	void Parse(int argc, char* argv[]);

	void ParseOptionNoArgument(Option option);
	void ParseOptionWithArgument(Option option, std::queue<std::string>& queue);

	// TODO I can refer to many things by handle or by option
	// Perhaps there is a way to avoid repeating the same mechanism
	// of refering by Option via getHandle()?

	Clause::Traits getClauseTraits(std::string handle);
	Clause::Traits getClauseTraitsByOption(Option option);

	int getOptionTimes(std::string handle);
	int getOptionTimesByOption(Option option);

	std::string getOptionArgument(std::string handle);

	std::vector<std::string> getOperands();
	
};
