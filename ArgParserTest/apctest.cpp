#include "ArgParser.h"

#include <iostream>
#include <string>

using namespace std;

void print(ArgParser& parser, string handle, bool argument_mandatory);
void print_operands(ArgParser& parser);

int main(int argc, char *argv[])
{
	ArgParser parser;

	parser.RegisterClause(ArgParser::Clause("handle-abc", { 'a', 'b', 'c' }, { "long-abc" }, { false, false }));
	parser.RegisterClause(ArgParser::Clause("handle-def", { 'd', 'e', 'f' }, { "long-def" }, { true, false }));
	parser.RegisterClause(ArgParser::Clause("handle-ghi", { 'g', 'h', 'i' }, { "long-ghi" }, { false, true }));
	parser.RegisterClause(ArgParser::Clause("handle-jkl", { 'j', 'k', 'l' }, { "long-jkl" }, { true, true }));

	try
	{
		parser.Parse(argc, argv);
	}
	catch (runtime_error exception)
	{
		cout << exception.what() << endl;
		return 1; // TODO proper exit code
	}
	print(parser, "handle-abc", false);
	print(parser, "handle-def", true);
	print(parser, "handle-ghi", false);
	print(parser, "handle-jkl", true);

	print_operands(parser);

	return 0;
}

void print(ArgParser& parser, string handle, bool argument_mandatory)
{
	cout << handle << ": " << parser.getOptionTimes(handle);
	if (argument_mandatory) cout << ", " << parser.getOptionArgument(handle);
	cout << endl;
}

void print_operands(ArgParser & parser)
{
	for (string operand : parser.getOperands())
	{
		cout << '"' << operand << "\"\n";
	}
}
